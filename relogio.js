function relogio() {
  let data = new Date();

  let hora = data.getHours();
  let minuto = data.getMinutes();
  let segundo = data.getSeconds();

  if (segundo < 10) {
    segundo = "0" + segundo
  }
  if (minuto < 10) {
    minuto = "0" + minuto
  }
  if (hora < 10) {
    hora = "0" + hora
  }

  document.getElementById('relogio').innerHTML = hora + ":" + minuto + ":" + segundo;
}

window.setInterval("relogio()", 1000);
